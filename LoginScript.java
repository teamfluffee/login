package scripts.modules.login;

import org.tribot.script.ScriptManifest;
import org.tribot.script.interfaces.Arguments;
import org.tribot.script.interfaces.Painting;
import org.tribot.script.interfaces.Starting;
import scripts.fluffeesapi.data.accounts.AccountDetails;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.Mission;
import scripts.fluffeesapi.scripting.frameworks.mission.scriptTypes.MissionScript;
import scripts.fluffeesapi.scripting.painting.scriptPaint.ScriptPaint;
import scripts.fluffeesapi.scripting.swingcomponents.gui.AbstractWizardGui;
import scripts.fluffeesapi.utilities.ArgumentUtilities;

import java.awt.*;
import java.util.HashMap;

@ScriptManifest(
        authors     = "Fluffee",
        category    = "Local Scripts",
        name        = "Fluffees Login",
        version     = 1.0,
        description = "Logs into an account using the following arguments. <br>" +
                "<b>accountEmail</b>:email_address;<b>accountPassword</b>:password;",
        gameMode = 1)

public class LoginScript extends MissionScript implements Painting, Arguments, Starting {

    private AccountDetails accountDetails;

    private ScriptPaint scriptPaint = new ScriptPaint.Builder(ScriptPaint.hex2Rgb("#ffb140"), "Login")
            .addField("Version", Double.toString(MANIFEST.version()))
            .build();

    @Override
    public void passArguments(HashMap<String, String> hashMap) {
        if (hashMap.isEmpty()) {
            this.hasArguments = false;
            return;
        }
        HashMap<String, String> arguments = ArgumentUtilities.get(hashMap);
        String accountEmail = arguments.get("accountEmail");
        String accountPassword = arguments.get("accountPassword");
        accountDetails = new AccountDetails(accountEmail, accountPassword);
        this.hasArguments = true;
    }

    @Override
    public ScriptPaint getScriptPaint() {
        return scriptPaint;
    }

    @Override
    public void onPaint(Graphics graphics) {
        scriptPaint.paint((Graphics2D) graphics, this.getRunningTime(), getPaintFields());
    }

    @Override
    public AbstractWizardGui getGUI() {
        return null;
    }

    @Override
    public void onStart() {
        this.setLoginBotState(false);
    }

    @Override
    public Mission getMission() {
        return new FluffeesLogin(accountDetails);
    }

}
