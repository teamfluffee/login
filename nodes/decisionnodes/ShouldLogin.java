package scripts.modules.login.nodes.decisionnodes;

import org.tribot.api2007.Login;
import scripts.modules.login.nodes.processnodes.ClickHereToPlay;
import scripts.modules.login.nodes.processnodes.LoginNode;
import scripts.modules.fluffeesapi.data.accounts.AccountDetails;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.ConstructorDecisionNode;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.FactoryDecisionNode;

public class ShouldLogin extends FactoryDecisionNode {

    public static final int LOBBY_INTERFACE_MASTER = 378;
    private AccountDetails accountDetails;

    @Override
    public boolean isValid() {
        return Login.getLoginState() != Login.STATE.INGAME;
    }

    @Override
    public void initializeNode() {
        setTrueNode(new LoginNode(accountDetails));
        setFalseNode(new ClickHereToPlay());
    }

    public static ShouldLogin create(AccountDetails accountDetails) {
        ShouldLogin shouldLogin = new ShouldLogin();
        shouldLogin.accountDetails = accountDetails;
        shouldLogin.initializeNode();
        return shouldLogin;
    }
}
