package scripts.modules.login.nodes.processnodes;

import org.tribot.api2007.Login;
import scripts.modules.fluffeesapi.data.accounts.AccountDetails;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;

public class LoginNode extends SuccessProcessNode {

    private AccountDetails accountDetails;

    public LoginNode(AccountDetails accountDetails) {
        this.accountDetails = accountDetails;
    }

    @Override
    public String getStatus() {
        return "Logging In";
    }

    @Override
    public void successExecute() {
        Login.login(accountDetails.getEmail(), accountDetails.getPassword());
    }
}
