package scripts.modules.login.nodes.processnodes;

import org.tribot.api.Clicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api2007.Interfaces;
import org.tribot.api2007.types.RSInterfaceChild;
import scripts.modules.login.nodes.decisionnodes.ShouldLogin;
import scripts.modules.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.SuccessProcessNode;
import scripts.modules.fluffeesapi.utilities.Conditions;

public class ClickHereToPlay extends SuccessProcessNode {

    private final int CLICK_HERE_BUTTON = 85;

    @Override
    public String getStatus() {
        return "Clicking Here to Play";
    }

    @Override
    public void successExecute() {
        RSInterfaceChild clickHereToPlay = Interfaces.get(ShouldLogin.LOBBY_INTERFACE_MASTER, CLICK_HERE_BUTTON);
        if (Interfaces.isInterfaceSubstantiated(clickHereToPlay)) {
            Clicking.click("", clickHereToPlay);
            Timing.waitCondition(Conditions.interfaceNotSubstantiated(clickHereToPlay), General.random(3000, 5000));
        }
    }
}
