package scripts.modules.login;

import scripts.modules.login.nodes.decisionnodes.ShouldLogin;
import scripts.fluffeesapi.data.accounts.AccountDetails;
import scripts.fluffeesapi.scripting.frameworks.mission.missiontypes.TreeMission;
import scripts.fluffeesapi.scripting.frameworks.modulardecisiontree.nodes.BaseDecisionNode;

public class Login implements TreeMission {

    private AccountDetails accountDetails;

    public Login(AccountDetails accountDetails) {
       this.accountDetails = accountDetails;
    }

    @Override
    public String getMissionName() {
        return "LoginNode";
    }

    @Override
    public boolean isMissionValid() {
        return org.tribot.api2007.Login.getLoginState() != org.tribot.api2007.Login.STATE.INGAME;
    }

    @Override
    public boolean isMissionCompleted() {
        return !isMissionValid();
    }

    @Override
    public BaseDecisionNode getTreeRoot() {
        return ShouldLogin.create(accountDetails);
    }

    @Override
    public MissionGameState getMissionGameState() {
        return MissionGameState.OUT_OF_GAME;
    }

}
